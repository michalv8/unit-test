<?php

declare(strict_types=1);

namespace App\Handler;

use App\Model\User;

class UserHandler
{
    public function modifyUser(User $user, string $username): User
    {
        $user->setUsername($username);

        return $user;
    }

    public function cloneUser(User $user): User
    {
        return clone $user;
    }
}