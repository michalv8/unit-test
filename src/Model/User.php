<?php

declare(strict_types=1);

namespace App\Model;

class User
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $username;

    public function __construct(string $firstName, string $lastName, string $username)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->username = $username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }
}